"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("Details", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      gameId: {
        allowNull: false,
        references: {
          model: {
            tableName: "Games",
            schema: "public",
          },
          key: "id",
        },
        type: Sequelize.INTEGER,
      },
      userId: {
        allowNull: false,
        references: {
          model: {
            tableName: "Users",
            schema: "public",
          },
          key: "id",
        },
        type: Sequelize.INTEGER,
      },
      score: {
        allowNull: true,
        type: Sequelize.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("Details");
  },
};
