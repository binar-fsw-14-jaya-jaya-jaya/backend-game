const cloudinary = require('cloudinary');

const auth = require('../controllers/authController');
const db = require('../models');

jest.mock('cloudinary');

const mockRequest = (body = {}) => {
  return { body };
};

const mockResponse = () => {
  const res = {};
  res.json = jest.fn().mockReturnValue(res);
  res.status = jest.fn().mockReturnValue(res);
  return res;
};

beforeAll(async () => {
  await db.sequelize.sync({ force: true });
});

describe('register function', () => {
  test('status code should 201 if successfully create user account', async () => {
    const req = mockRequest({
      first_name: 'binar',
      last_name: 'student',
      username: 'binar',
      email: 'binar@gmail.com',
      password: '123456',
      avatar: undefined,
    });

    const res = mockResponse();

    await cloudinary.v2.uploader.upload.mockResolvedValue({
      public_id: null,
      secure_url: null,
    });

    await auth.register(req, res);

    expect(res.status).toHaveBeenCalledWith(201);
    expect(res.json).toHaveBeenCalledWith({
      result: 'success',
      message: 'Congratulations, your account has been successfully created.',
      user: {
        first_name: 'binar',
        last_name: 'student',
        email: 'binar@gmail.com',
        username: 'binar',
        accessToken: expect.any(String),
      },
    });
  });

  test('status code should 409 if the username has already registered', async () => {
    const req = mockRequest({
      first_name: 'binar1',
      last_name: 'student1',
      username: 'binar',
      email: 'binar1@gmail.com',
      password: '123456',
    });

    const res = mockResponse();
    await auth.register(req, res);

    expect(res.status).toHaveBeenCalledWith(409);
    expect(res.json).toHaveBeenCalledWith({
      result: 'failed',
      message: 'The username is already registered',
    });
  });

  test('status code should 400 if email is already registered', async () => {
    const req = mockRequest({
      first_name: 'binar1',
      last_name: 'student1',
      username: 'binar1',
      email: 'binar@gmail.com',
      password: '123456',
    });

    const res = mockResponse();
    await auth.register(req, res);

    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.json).toHaveBeenCalledWith({
      result: 'failed',
      message:
        'Registration Failed, Please go back and double check your information and make sure that is valid',
      error: 'the email is already registered',
    });
  });

  test('status code should 400 if the email format is invalid', async () => {
    const req = mockRequest({
      first_name: 'binar1',
      last_name: 'student1',
      username: 'binar1',
      email: 'binar1.@gmail.com',
      password: '123456',
    });

    const res = mockResponse();
    await auth.register(req, res);

    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.json).toHaveBeenCalledWith({
      result: 'failed',
      message:
        'Registration Failed, Please go back and double check your information and make sure that is valid',
      error: 'invalid email format',
    });
  });

  test('status code should 400 if the length of password is less than 6 characters', async () => {
    const req = mockRequest({
      first_name: 'binar1',
      last_name: 'student1',
      username: 'binar1',
      email: 'binar1@gmail.com',
      password: '12345',
    });

    const res = mockResponse();
    await auth.register(req, res);

    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.json).toHaveBeenCalledWith({
      result: 'failed',
      message:
        'Registration Failed, Please go back and double check your information and make sure that is valid',
      error: 'password at least six characters',
    });
  });
});

describe('login function', () => {
  test('status code should 200 if login successfully', async () => {
    const req = mockRequest({
      username: 'binar',
      password: '123456',
    });

    const res = mockResponse();
    await auth.login(req, res);

    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledWith({
      result: 'success',
      message: 'Login Successfully',
      user: {
        id: expect.any(Number),
        first_name: 'binar',
        last_name: 'student',
        email: 'binar@gmail.com',
        username: 'binar',
        accessToken: expect.any(String),
      },
    });
  });

  test('status code should 404 if user not found', async () => {
    const req = mockRequest({
      username: 'binar1',
      password: '123456',
    });

    const res = mockResponse();
    await auth.login(req, res);

    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.json).toHaveBeenCalledWith({
      result: 'failed',
      message: 'User Not Found',
    });
  });

  test('status code should 401 if passed password does not match stored password', async () => {
    const req = mockRequest({
      username: 'binar',
      password: '1234567',
    });

    const res = mockResponse();
    await auth.login(req, res);

    expect(res.status).toHaveBeenCalledWith(401);
    expect(res.json).toHaveBeenCalledWith({
      result: 'failed',
      message: 'Please enter a valid username or password',
    });
  });
});

afterAll(async () => {
  await db.sequelize.close();
});
