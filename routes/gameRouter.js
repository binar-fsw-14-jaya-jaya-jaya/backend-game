const router = require('express').Router();
const gameController = require('../controllers/gameController');

router.get('/', gameController.findAll);
router.get('/:slug', gameController.findOne);
router.get('/:id/leaderboard', gameController.getLeaderboard);

module.exports = router;
