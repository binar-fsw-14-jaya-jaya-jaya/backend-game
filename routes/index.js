const router = require('express').Router();
const authRouter = require('./authRouter');
const userRouter = require('./userRouter');
const myProfileRouter = require('./myProfileRouter');
const gameRouter = require('./gameRouter');
const videoRouter = require('./videoRouter');
const gameController = require('../controllers/gameController');

const restrict = require('../middlewares/restrict');

router.use('/', authRouter);
router.use('/users', userRouter);
router.use('/myprofile', myProfileRouter);
router.use('/games', gameRouter);
router.use('/videos', videoRouter);
router.put('/score', restrict, gameController.updateScore);

module.exports = router;
