const router = require('express').Router();
const userController = require('../controllers/userController');

router.get('/leaderboard', userController.getLeaderboard);
router.get('/:username', userController.findOne);

module.exports = router;
