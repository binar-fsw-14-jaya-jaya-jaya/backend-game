const router = require('express').Router();
const authController = require('../controllers/authController');
const upload = require('../middlewares/multer');

router.get('/', authController.index);
router.post('/register', upload.single('avatar'), authController.register);
router.post('/login', authController.login);

module.exports = router;
