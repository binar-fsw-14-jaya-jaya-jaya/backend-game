const router = require('express').Router();
const profileController = require('../controllers/profileController');

// middleware
const restrict = require('../middlewares/restrict');
const upload = require('../middlewares/multer');

router.use(restrict);

router.get('/', profileController.getMyProfile);
router.put(
  '/update',
  upload.single('avatar'),
  profileController.updateMyProfile
);

module.exports = router;
