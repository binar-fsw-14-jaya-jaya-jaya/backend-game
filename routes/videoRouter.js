const router = require('express').Router();
const videoController = require('../controllers/videoController');
const restrict = require('../middlewares/restrict');

router.get('/', videoController.findAll);

router.use(restrict);

router.post('/upload', videoController.upload);
router.put('/:id/update', videoController.updateOne);
router.delete('/:id/delete', videoController.deleteOne);

module.exports = router;
