const { Video } = require('../models');

const findAll = (req, res) => {
  Video.findAll()
    .then((videos) => {
      res.status(200).json({
        result: 'success',
        message: 'Videos successfully retrieved',
        videos,
      });
    })
    .catch((err) => {
      res.status(500).json({
        result: 'failed',
        message: 'Some error occurred while retriving videos',
        error: err.message,
      });
    });
};

const upload = async (req, res) => {
  const { title, slug, url, poster } = req.body;
  console.log({
    title,
    slug,
    url,
    poster,
  });

  try {
    const createdData = await Video.create({
      title,
      slug,
      url,
      poster,
    });
    res.status(201).json({
      result: 'success',
      message: 'Congratulations, your video has been successfully uploaded.',
      data: createdData,
    });
  } catch (err) {
    res.status(400).json({
      result: 'failed',
      message: 'Upload failed',
      error: err.errors[0].message,
    });
  }
};

const updateOne = async (req, res) => {
  const { title, slug, url, poster } = req.body;
  console.log({
    title,
    slug,
    url,
    poster,
  });

  try {
    const video = await Video.findOne({
      where: { id: req.params.id },
      attributes: {
        exclude: ['createdAt', 'updatedAt'],
      },
    });
    if (!video) {
      return res.status(404).json({
        result: 'failed',
        message: 'The video that you want to update is not found',
      });
    }
    const updatedData = await Video.update(
      {
        title,
        slug,
        url,
        poster,
      },
      {
        where: { id: req.params.id },
        returning: true,
      }
    );
    res.status(201).json({
      result: 'success',
      message: 'Congratulations, your video has been successfully updated.',
      data: updatedData,
    });
  } catch (err) {
    res.status(500).json({
      result: 'failed',
      message: 'some error occured while updating the video',
      error: err.message,
    });
  }
};

const deleteOne = async (req, res) => {
  try {
    const video = await Video.findOne({
      where: { id: req.params.id },
      attributes: {
        exclude: ['createdAt', 'updatedAt'],
      },
    });
    if (!video) {
      return res.status(404).json({
        result: 'failed',
        message: 'The video that you want to delete is not found',
      });
    }
    await Video.destroy({
      where: { id: req.params.id },
      returning: true,
    });
    res.status(200).json({
      result: 'Success',
      message: 'Your video has been successfully deleted',
      video,
    });
  } catch (err) {
    res.status(500).json({
      result: 'Failed',
      message: 'Some error occured while updating your data',
      error: err.message,
    });
  }
};

module.exports = { findAll, upload, updateOne, deleteOne };
