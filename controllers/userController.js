const { User } = require('../models');

const findOne = (req, res) => {
  User.findOne({
    attributes: { exclude: ['password', 'createdAt', 'updatedAt'] },
    where: {
      username: req.params.username,
    },
  })
    .then((data) => {
      if (!data) {
        return res.status(404).json({
          result: 'failed',
          message: 'user not registered',
        });
      }
      res.status(200).json({
        result: 'success',
        message: 'successfully retrieve data',
        user: data,
      });
    })
    .catch((err) => {
      res.status(500).json({
        result: 'failed',
        message: 'some error occured while retrieving user data',
        error: err.message,
      });
    });
};

const getLeaderboard = (req, res) => {
  User.findAll({
    attributes: {
      exclude: [
        'bio',
        'email',
        'social_media_url',
        'password',
        'createdAt',
        'updatedAt',
      ],
    },
    order: [['total_score', 'DESC']],
  })
    .then((data) => {
      res.status(200).json({
        result: 'success',
        message: 'successfully retrieve data',
        users: data,
      });
    })
    .catch((err) => {
      res.status(500).json({
        result: 'failed',
        message: 'some error occured while retrieving the data',
        error: err.message,
      });
    });
};
module.exports = { findOne, getLeaderboard };
