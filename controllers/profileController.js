/* eslint-disable vars-on-top, no-restricted-syntax */
const cloudinary = require('../helpers/cloudinary');
const { User } = require('../models');

function removeEmpty(obj) {
  for (var propName in obj) {
    if (
      obj[propName] === null ||
      obj[propName] === undefined ||
      obj[propName].length === 0
    ) {
      delete obj[propName];
    }
  }
  return obj;
}

module.exports = {
  getMyProfile: async (req, res) => {
    try {
      const data = await User.findOne({
        where: {
          id: req.user.id,
        },
        attributes: { exclude: ['password', 'createdAt', 'updatedAt'] },
      });
      return res.status(200).json({
        result: 'succes',
        message: 'succesfully retrieve data',
        profile: data,
      });
    } catch (err) {
      return res.status(500).json({
        result: 'failed',
        message: 'Some error occured while retrieving your profile',
        error: err.message,
      });
    }
  },

  updateMyProfile: async (req, res) => {
    try {
      const payload = removeEmpty(req.body);

      let user = User.findOne({
        where: { id: req.user.id },
      });

      console.log(user);

      if (req.file) {
        if (req.file.path) {
          const image_id = user.avatar_public_id;

          if (image_id) {
            await cloudinary.uploader.destroy(image_id);
          }

          const result = await cloudinary.uploader.upload(req.file.path, {
            folder: 'binar_challenge/avatar',
            width: '150',
            crop: 'scale',
          });

          user = await User.update(
            {
              ...payload,
              avatar_public_id: result.public_id,
              avatar_url: result.secure_url,
            },
            {
              where: { id: req.user.id },
              returning: true,
            }
          );

          return res.status(201).json({
            result: 'success',
            message: 'Your profile has been successfully updated',
            profile: user,
          });
        }
      }

      user = await User.update(payload, {
        where: { id: req.user.id },
        returning: true,
      });

      return res.status(201).json({
        result: 'success',
        message: 'Your profile has been successfully updated',
        profile: user,
      });
    } catch (err) {
      res.status(500).json({
        result: 'failed',
        message: 'some error occured while updating your profile',
        error: err.message,
      });
    }
  },
};
