/* eslint-disable no-redeclare, vars-on-top */
const { comparePassword } = require('../helpers/bcrypt');
const { User } = require('../models');
const { generateToken } = require('../helpers/jwt');
const cloudinary = require('../helpers/cloudinary');

const index = async (req, res) => {
  res.send('Something is cooking inside this kitchen');
};

const register = async (req, res) => {
  const { first_name, last_name, email, username, password } = req.body;
  let result = {
    public_id: null,
    secure_url: null,
  };
  try {
    const user = await User.findOne({
      where: { username },
    });

    if (user) {
      return res.status(409).json({
        result: 'failed',
        message: 'The username is already registered',
      });
    }
    if (req.file) {
      result = await cloudinary.uploader.upload(req.file.path, {
        folder: 'binar_challenge/avatar',
        width: '150',
        crop: 'scale',
      });
    }

    // throw new Error('another error, e.g internal server error');
  } catch (err) {
    return res.status(500).json({
      result: 'failed',
      message: 'Internal Server Error',
      error: err.message,
    });
  }

  try {
    var user = await User.create({
      first_name,
      last_name,
      email,
      username,
      password,
      avatar_public_id: result.public_id,
      avatar_url: result.secure_url,
    });
    var { user } = User.format(user);
    return res.status(201).json({
      result: 'success',
      message: 'Congratulations, your account has been successfully created.',
      user: {
        first_name,
        last_name,
        email,
        username,
        accessToken: generateToken({
          id: user.id,
          email: user.email,
          username: user.username,
        }),
      },
    });
  } catch (err) {
    return res.status(400).json({
      result: 'failed',
      message:
        'Registration Failed, Please go back and double check your information and make sure that is valid',
      error: err.errors[0].message,
    });
  }
};

const login = async (req, res) => {
  const { username, password } = req.body;

  try {
    const user = await User.findOne({
      where: { username },
    });
    console.log(user);

    if (!user) {
      return res.status(404).json({
        result: 'failed',
        message: 'User Not Found',
      });
    }

    const match = comparePassword(password, user.password);
    if (!match) {
      return res.status(401).json({
        result: 'failed',
        message: 'Please enter a valid username or password',
      });
    }

    return res.status(200).json(User.format(user));
  } catch (err) {
    return res.status(500).json({
      result: 'failed',
      message: 'Internal Server Error',
      error: err.message,
    });
  }
};

module.exports = { index, register, login };
