const { Game, Detail, User } = require('../models');

module.exports = {
  findAll: async (req, res) => {
    try {
      const data = await Game.findAll();
      return res.status(200).json({
        result: 'success',
        message: 'successfully retrieve the data',
        games: data,
      });
    } catch (err) {
      return res.status(500).json({
        result: 'failed',
        message: 'Some error occurred while retriving the Game',
        error: err.message,
      });
    }
  },

  findOne: async (req, res) => {
    try {
      const data = await Game.findOne({
        where: {
          slug: req.params.slug,
        },
      });
      if (!data) {
        return res.status(404).json({
          result: 'failed',
          message: 'Game is not registered',
        });
      }
      return res.status(200).json({
        result: 'success',
        message: 'successfully retrieve the data',
        game: data,
      });
    } catch (err) {
      return res.status(500).json({
        result: 'failed',
        message: 'Some error occured while retrieving game',
        error: err.message,
      });
    }
  },

  getLeaderboard: async (req, res) => {
    try {
      const data = await Detail.findAll({
        order: [['score', 'DESC']],
        where: {
          gameId: req.params.id,
        },
        attributes: ['gameId', 'userId', 'score'],
        include: {
          model: User,
          as: 'detail_user',
          attributes: [
            'first_name',
            'last_name',
            'username',
            'email',
            'avatar_public_id',
            'avatar_url',
          ],
        },
      });
      return res.status(200).json({
        result: 'succes',
        message: 'succesfully retrieve data',
        users: data,
      });
    } catch (err) {
      return res.status(500).json({
        result: 'failed',
        message: 'Some error occured while retrieving leaderboard',
        error: err.message,
      });
    }
  },

  updateScore: async (req, res) => {
    try {
      let detail = await Detail.findOne({
        attributes: ['score'],
        where: {
          userId: req.user.id,
          gameId: req.body.gameId,
        },
      });
      let user = await User.findOne({
        attributes: ['id', 'total_score'],
        where: {
          id: req.user.id,
        },
      });
      if (detail) {
        detail = await Detail.update(
          {
            score: parseInt(req.body.score) + detail.score,
          },
          {
            where: {
              userId: req.user.id,
              gameId: req.body.gameId,
            },
            returning: true,
          }
        );
        user = await user.update(
          {
            total_score: parseInt(req.body.score) + user.total_score,
          },
          {
            where: {
              id: req.user.id,
            },
            returning: true,
          }
        );
        return res.status(200).json({
          result: 'success',
          message: 'The score player has been successfully updated',
          user: {
            score: detail[1][0].score,
            totalScore: user.total_score,
          },
        });
      }
    } catch (error) {
      return res.status(500).json({
        result: 'failed',
        message: 'some error occured while updating score',
        error: error.message,
      });
    }

    try {
      let detail = await Detail.findOne({
        attributes: ['score'],
        where: {
          userId: req.user.id,
          gameId: req.body.gameId,
        },
      });
      let user = await User.findOne({
        attributes: ['id', 'total_score'],
        where: {
          id: req.user.id,
        },
      });
      if (!detail) {
        detail = await Detail.create({
          userId: req.user.id,
          gameId: req.body.gameId,
          score: req.body.score,
        });
        user = await user.update(
          {
            total_score: parseInt(req.body.score) + user.total_score,
          },
          {
            where: {
              id: req.user.id,
            },
            returning: true,
          }
        );
        return res.status(201).json({
          result: 'success',
          message: 'The player score has been successfully added',
          user: {
            score: detail.score,
            totalScore: user.total_score,
          },
        });
      }
    } catch (error) {
      return res.status(501).json({
        result: 'failed',
        message: 'some error occured while adding score',
        error: error.message,
      });
    }
  },
};
