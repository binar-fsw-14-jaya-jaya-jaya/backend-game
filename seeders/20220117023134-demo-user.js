"use strict";
const { hashPassword } = require("../helpers/bcrypt");
module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert("Users", [
      {
        first_name: "greg",
        last_name: "dohok",
        email: "greg@gmail.com",
        username: "greg",
        password: hashPassword("123456"),
        total_score: "2",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        first_name: "greg1",
        last_name: "dohok1",
        email: "greg1@gmail.com",
        username: "greg1",
        password: hashPassword("123456"),
        total_score: "3",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete("Users", null, {});
  },
};
