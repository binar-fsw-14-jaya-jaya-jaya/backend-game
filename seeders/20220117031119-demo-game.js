'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert('Games', [
      //game populer
      {
        name: 'Rock Paper Scissors',
        slug: 'rock-paper-scissors',
        description: `Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                      Repudiandae quisquam itaque amet eos commodi libero vero,
                      perferendis expedita unde ipsum id cumque distinctio nostrum,
                      alias earum error et blanditiis fugit.`,
        category: 'traditional',
        thumbnail_url:
          'https://cdn.pixabay.com/photo/2013/07/12/15/02/fingers-149296__340.png',
        play_count: 600,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Pac-Man',
        slug: 'pac-man',
        description: `Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                      Repudiandae quisquam itaque amet eos commodi libero vero,
                      perferendis expedita unde ipsum id cumque distinctio nostrum,
                      alias earum error et blanditiis fugit.`,
        category: 'adventure',
        thumbnail_url: '',
        play_count: 580,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Minecraft',
        slug: 'minecraft',
        description: `Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                      Repudiandae quisquam itaque amet eos commodi libero vero,
                      perferendis expedita unde ipsum id cumque distinctio nostrum,
                      alias earum error et blanditiis fugit.`,
        category: 'adventure',
        play_count: 480,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Grand Theft Auto V',
        slug: 'grand-theft-auto-v',
        description: `Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                      Repudiandae quisquam itaque amet eos commodi libero vero,
                      perferendis expedita unde ipsum id cumque distinctio nostrum,
                      alias earum error et blanditiis fugit.`,
        category: 'adventure',
        play_count: 450,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Metal Gear Solid V',
        slug: 'metal-gear-solid-v',
        description: `Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                      Repudiandae quisquam itaque amet eos commodi libero vero,
                      perferendis expedita unde ipsum id cumque distinctio nostrum,
                      alias earum error et blanditiis fugit.`,
        category: 'adventure',
        play_count: 300,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Tetris',
        slug: 'tetris',
        description: `Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                      Repudiandae quisquam itaque amet eos commodi libero vero,
                      perferendis expedita unde ipsum id cumque distinctio nostrum,
                      alias earum error et blanditiis fugit.`,
        category: 'adventure',
        play_count: 400,
        createdAt: new Date(),
        updatedAt: new Date(),
      },

      //game new
      {
        name: 'Red Dead Redemption 2',
        slug: 'red-Dead-redemption-2',
        description: `Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                      Repudiandae quisquam itaque amet eos commodi libero vero,
                      perferendis expedita unde ipsum id cumque distinctio nostrum,
                      alias earum error et blanditiis fugit.`,
        category: 'action',
        play_count: 15,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Moto Gp 20',
        slug: 'moto-gp-20',
        description: `Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                      Repudiandae quisquam itaque amet eos commodi libero vero,
                      perferendis expedita unde ipsum id cumque distinctio nostrum,
                      alias earum error et blanditiis fugit.`,
        category: 'action',
        play_count: 35,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Super Bomberman R Online',
        slug: 'super-bomberman-r-online',
        description: `Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                      Repudiandae quisquam itaque amet eos commodi libero vero,
                      perferendis expedita unde ipsum id cumque distinctio nostrum,
                      alias earum error et blanditiis fugit.`,
        category: 'action',
        play_count: 60,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Getsu Fuma Den',
        slug: 'getsu-fuma-den',
        description: `Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                      Repudiandae quisquam itaque amet eos commodi libero vero,
                      perferendis expedita unde ipsum id cumque distinctio nostrum,
                      alias earum error et blanditiis fugit.`,
        category: 'action',
        play_count: 25,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Castlevania Advance Collection',
        slug: 'castlevania-advance-collection',
        description: `Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                      Repudiandae quisquam itaque amet eos commodi libero vero,
                      perferendis expedita unde ipsum id cumque distinctio nostrum,
                      alias earum error et blanditiis fugit.`,
        category: 'action',
        play_count: 15,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Beat Arena',
        slug: 'beat-arena',
        description: `Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                      Repudiandae quisquam itaque amet eos commodi libero vero,
                      perferendis expedita unde ipsum id cumque distinctio nostrum,
                      alias earum error et blanditiis fugit.`,
        category: 'action',
        play_count: 45,
        createdAt: new Date(),
        updatedAt: new Date(),
      },

      {
        name: 'Red Dead Redemption 2',
        slug: 'red-Dead-redemption-2',
        description: `Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                      Repudiandae quisquam itaque amet eos commodi libero vero,
                      perferendis expedita unde ipsum id cumque distinctio nostrum,
                      alias earum error et blanditiis fugit.`,
        category: 'action',
        play_count: 76,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Moto Gp 20',
        slug: 'moto-gp-20',
        description: `Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                      Repudiandae quisquam itaque amet eos commodi libero vero,
                      perferendis expedita unde ipsum id cumque distinctio nostrum,
                      alias earum error et blanditiis fugit.`,
        category: 'action',
        play_count: 80,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Super Bomberman R Online',
        slug: 'super-bomberman-r-online',
        description: `Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                      Repudiandae quisquam itaque amet eos commodi libero vero,
                      perferendis expedita unde ipsum id cumque distinctio nostrum,
                      alias earum error et blanditiis fugit.`,
        category: 'action',
        play_count: 23,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Getsu Fuma Den',
        slug: 'getsu-fuma-den',
        description: `Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                      Repudiandae quisquam itaque amet eos commodi libero vero,
                      perferendis expedita unde ipsum id cumque distinctio nostrum,
                      alias earum error et blanditiis fugit.`,
        category: 'action',
        play_count: 22,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Castlevania Advance Collection',
        slug: 'castlevania-advance-collection',
        description: `Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                      Repudiandae quisquam itaque amet eos commodi libero vero,
                      perferendis expedita unde ipsum id cumque distinctio nostrum,
                      alias earum error et blanditiis fugit.`,
        category: 'action',
        play_count: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Beat Arena',
        slug: 'beat-arena',
        description: `Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                      Repudiandae quisquam itaque amet eos commodi libero vero,
                      perferendis expedita unde ipsum id cumque distinctio nostrum,
                      alias earum error et blanditiis fugit.`,
        category: 'action',
        play_count: 5,
        createdAt: new Date(),
        updatedAt: new Date(),
      },

      //game sport
      {
        name: 'Pes 2017',
        slug: 'pes-2017',
        description: `Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                      Repudiandae quisquam itaque amet eos commodi libero vero,
                      perferendis expedita unde ipsum id cumque distinctio nostrum,
                      alias earum error et blanditiis fugit.`,
        category: 'sport',
        play_count: 80,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'eFootball PES 2021 Mobile',
        slug: 'efootball-pes-2021-mobile',
        description: `Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                      Repudiandae quisquam itaque amet eos commodi libero vero,
                      perferendis expedita unde ipsum id cumque distinctio nostrum,
                      alias earum error et blanditiis fugit.`,
        category: 'sport',
        play_count: 12,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Grand Turismo',
        slug: 'grand-turismo',
        description: `Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                      Repudiandae quisquam itaque amet eos commodi libero vero,
                      perferendis expedita unde ipsum id cumque distinctio nostrum,
                      alias earum error et blanditiis fugit.`,
        category: 'sport',
        play_count: 60,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Downhill',
        slug: 'Downhill',
        description: `Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                      Repudiandae quisquam itaque amet eos commodi libero vero,
                      perferendis expedita unde ipsum id cumque distinctio nostrum,
                      alias earum error et blanditiis fugit.`,
        category: 'sport',
        play_count: 80,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Badminton',
        slug: 'badminton',
        description: `Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                      Repudiandae quisquam itaque amet eos commodi libero vero,
                      perferendis expedita unde ipsum id cumque distinctio nostrum,
                      alias earum error et blanditiis fugit.`,
        category: 'sport',
        play_count: 45,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        name: 'Basket Ball',
        slug: 'basket ball',
        description: `Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                      Repudiandae quisquam itaque amet eos commodi libero vero,
                      perferendis expedita unde ipsum id cumque distinctio nostrum,
                      alias earum error et blanditiis fugit.`,
        category: 'sport',
        play_count: 68,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Games', null, {});
  },
};
