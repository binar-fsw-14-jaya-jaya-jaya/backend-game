'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.bulkInsert('Videos', [
      {
        title: 'Sintel Trailer',
        slug: 'sintel-trailer',
        url: 'http://media.w3.org/2010/05/sintel/trailer.mp4',
        poster: 'https://cdn.pixabay.com/photo/2018/02/21/11/02/adult-3170055_1280.jpg',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        title: 'Bunny Trailer',
        slug: 'bunny-trailer',
        url: 'http://media.w3.org/2010/05/bunny/trailer.mp4',
        poster: 'https://cdn.pixabay.com/photo/2015/01/08/18/24/children-593313_1280.jpg',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        title: 'Bunny Movie',
        slug: 'bunny-movie',
        url: 'http://media.w3.org/2010/05/bunny/movie.mp4',
        poster: 'https://cdn.pixabay.com/photo/2016/11/14/05/21/children-1822688_1280.jpg',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        title: 'Test Movie',
        slug: 'test-movie',
        url: 'http://media.w3.org/2010/05/video/movie_300.webm',
        poster: 'https://cdn.pixabay.com/photo/2016/11/15/07/09/photo-manipulation-1825450__480.jpg',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Videos', null, {});
  },
};
