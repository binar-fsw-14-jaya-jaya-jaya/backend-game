# BackEnd_Game

## Note

- bash path `/api`

## Setup for development

- clone repo
- instal dependencies `npm install`
- sesuaikan isi file `.env` dengan database masing masing
- create database `sequelize db:create`
- jalankan migration `sequelize db:migrate`
- jalankan seeder `sequelize db:seed:all`
- jalankan aplikasi `npm start`

## Available Script

- `npm start` to run app
- `npm run lint` to lint all the file with .js extension
- `npm run test` to do unit testing

## Access swagger documentation

- pastikan telah menginstall swagger-ui
- jika belum, jalankan `npm install`
- masuk ke endpoint `/docs` untuk melihat dokumentasi swegger
