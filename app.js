/* eslint-disable linebreak-style */

require('dotenv').config();
const express = require('express');
const swaggerUI = require('swagger-ui-express');
const cors = require('cors');

const router = require('./routes');
const passport = require('./lib/passport');

const swaggerJSON = require('./swagger.json');

const port = process.env.PORT;
const app = express();

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use('/docs', swaggerUI.serve, swaggerUI.setup(swaggerJSON));
app.use(passport.initialize());
app.use('/api', router);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use((err, req, res) => {
    res.status(err.status || 500).json({
      message: err.message,
      error: err,
    });
  });
}

// production error handler
app.use((err, req, res) => {
  res.status(err.status || 500).json({
    message: err.message,
    error: err,
  });
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
