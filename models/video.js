'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Video extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate() {
      // define association here
    }
  }
  Video.init(
    {
      title: DataTypes.STRING,
      slug: DataTypes.STRING,
      url: DataTypes.STRING,
      poster: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'Video',
    }
  );
  return Video;
};
