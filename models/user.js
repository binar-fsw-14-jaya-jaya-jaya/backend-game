'use strict';
const { Model } = require('sequelize');
const { hashPassword } = require('../helpers/bcrypt');
const { generateToken } = require('../helpers/jwt');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.Detail, {
        foreignKey: 'userId',
        sourceKey: 'id',
        as: 'detail_user',
      });
    }
    static format = (user) => {
      const { id, first_name, last_name, email, username } = user;

      const payload = {
        id,
        email,
        username,
      };

      return {
        result: 'success',
        message: 'Login Successfully',
        user: {
          id,
          first_name,
          last_name,
          email,
          username,
          accessToken: generateToken(payload),
        },
      };
    };
  }
  User.init(
    {
      first_name: DataTypes.STRING,
      last_name: DataTypes.STRING,
      email: {
        type: DataTypes.STRING,
        validate: {
          isEmail: {
            args: true,
            msg: 'invalid email format',
          },
        },
        unique: {
          args: true,
          msg: 'the email is already registered',
        },
      },
      username: {
        type: DataTypes.STRING,
        unique: {
          args: true,
          msg: 'The username is already registered',
        },
      },
      password: {
        type: DataTypes.STRING,
        validate: {
          len: {
            args: [6],
            msg: 'password at least six characters',
          },
        },
      },
      total_score: DataTypes.INTEGER,
      bio: DataTypes.STRING,
      location: DataTypes.STRING,
      social_media_url: DataTypes.STRING,
      avatar_public_id: DataTypes.STRING,
      avatar_url: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'User',
      hooks: {
        beforeCreate: (user) => {
          user.password = hashPassword(user.password);
        },
      },
    }
  );
  User.addHook('beforeBulkUpdate', (user) => {
    user.attributes.password = hashPassword(user.attributes.password);
  });
  return User;
};
